import React, { Component } from "react";
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { View, Text } from "react-native";
import HomeView from './components/HomeView';
import SearchView from "./components/SearchView";
import PlayerView from "./player/Player";
import Icon from "react-native-vector-icons/dist/FontAwesome";

const SView = createStackNavigator({
    Search: { screen: SearchView, title: 'Search' },
    Player: { screen: PlayerView, title: 'Player' },
});

const AppView = createBottomTabNavigator({
    Home: { screen: HomeView },
    Search: { screen: SView },
}, {
    tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        activeBackgroundColor: '#dddddd',
    },
    navigationOptions: ({navigation}) => ({
        tabBarIcon: ({focused, tintColor}) => {
            let { routeName } = navigation.state;
            let iconName = '';
            if(routeName === 'Home'){
                iconName = 'home';
            }
            if(routeName === 'Search'){
                iconName = 'search';
            }
            return <Icon name={iconName} size={20} color={tintColor} />;
        },
    }),
    // tabBarComponent: (xxx, yyy, zzz) => {      custom bottom tab
        // console.log('tabBarComponent', xxx, yyy, zzz);
        
        // return <View><Text>XYZ</Text></View>
    // },
});
export default AppView;