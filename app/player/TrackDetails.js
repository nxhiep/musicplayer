import React, { Component } from "react";

import {
	View,
	Text,
	StyleSheet,
	Image,
	TouchableOpacity,
	Modal,
	FlatList,
	Linking
} from "react-native";

export default class TrackDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false
		};
	}

	setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	renderDownloadItem(item) {
		return (
			<TouchableOpacity
				style={styles.itemDownload}
				onPress={() => {
					Linking.canOpenURL(item.url).then(supported => {
						if (supported) {
							Linking.openURL(item.url);
						} else {
							console.log("Don't know how to open URI: " + item.url);
						}
					});
				}}
			>
				<Text style={{ color: "white", textAlign: "center" }}>{item.name}</Text>
			</TouchableOpacity>
		);
	}

	render() {
		let { title, artist, urls, onAddPress, onMorePress, onTitlePress, onArtistPress } = this.props;
		// console.log(urls);
		const moreDownload = this.props.isLocal ? null : (
			<View>
				<TouchableOpacity
					onPress={() => {
						this.setModalVisible(true);
					}}
				>
					<View style={styles.moreButton}>
						<Image
							style={styles.moreButtonIcon}
							source={require("../../img/ic_more_horiz_white.png")}
						/>
					</View>
				</TouchableOpacity>
				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						Alert.alert("Modal has been closed.");
					}}
				>
					<TouchableOpacity
						style={styles.modal}
						onPress={() => this.setModalVisible(false)}
					>
						<View style={styles.downloadPanel}>
							<Text style={styles.titleDownload}>Chọn chất lượng</Text>
							<FlatList
								keyExtractor={item => item.name}
								data={urls}
								renderItem={({ item }) => this.renderDownloadItem(item)}
							/>
						</View>
					</TouchableOpacity>
				</Modal>
			</View>
		);
		return (
			<View style={styles.container}>
				<TouchableOpacity onPress={onAddPress}>
					<Image
						style={styles.button}
						source={require("../../img/ic_add_circle_outline_white.png")}
					/>
				</TouchableOpacity>
				<View style={styles.detailsWrapper}>
					<Text style={styles.title} onPress={onTitlePress}>
						{title}
					</Text>
					<Text style={styles.artist} onPress={onArtistPress}>
						{artist}
					</Text>
				</View>
				{ moreDownload }
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		paddingTop: 24,
		flexDirection: "row",
		paddingLeft: 20,
		alignItems: "center",
		paddingRight: 20
	},
	detailsWrapper: {
		justifyContent: "center",
		alignItems: "center",
		flex: 1
	},
	title: {
		fontSize: 16,
		fontWeight: "bold",
		color: "white",
		textAlign: "center"
	},
	artist: {
		color: "rgba(255, 255, 255, 0.72)",
		fontSize: 12,
		marginTop: 4
	},
	button: {
		opacity: 0.72
	},
	moreButton: {
		borderColor: "rgb(255, 255, 255)",
		borderWidth: 2,
		opacity: 0.72,
		borderRadius: 10,
		width: 20,
		height: 20,
		alignItems: "center",
		justifyContent: "center"
	},
	modal: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "rgba(0, 0, 0, 0.5)"
	},
	moreButtonIcon: {
		height: 17,
		width: 17
	},
	downloadPanel: {
		width: 150,
		backgroundColor: "white",
		borderRadius: 5,
		padding: 10,
		paddingLeft: 20,
		paddingRight: 20
	},
	titleDownload: {
		borderBottomWidth: 1,
		borderColor: "#555555",
		marginBottom: 10,
		textAlign: "center"
	},
	itemDownload: {
		borderRadius: 10,
		backgroundColor: "#00a99d",
		padding: 10,
		marginBottom: 10
	}
});
