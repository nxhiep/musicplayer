import React, { Component } from "react";
import { View, Text, StatusBar } from "react-native";
import Header from "./Header";
import AlbumArt from "./AlbumArt";
import TrackDetails from "./TrackDetails";
import SeekBar from "./SeekBar";
import Controls from "./Controls";
import Video from "react-native-video";
import * as Api from "../../utils/Api";
import * as Utils from "../../utils/Utils";

export default class Player extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam("item").name,
		};
	};

	constructor(props) {
		super(props);
		console.log('Player', props);
		
		this.state = {
			isLocal: props.navigation.state.params.isLocal,
			paused: !props.navigation.state.params.play,
			totalLength: 1,
			currentPosition: 0,
			selectedTrack: props.navigation.state.params.selectedTrack,
			repeatOn: false,
			shuffleOn: false,
			tracks: props.navigation.state.params.tracks,
			status: "loading",
			track: props.navigation.state.params.item
		};
		this.loadSong.bind(this);
	}

	componentDidMount() {
		this.loadSong();
	}

	loadSong() {
		const track = this.state.tracks[this.state.selectedTrack];
		if(this.state.isLocal){
			this.setState({ status: "success", track: track });
			return;
		}
		if (Utils.isTest) {
			this.setState({ status: "loading" });
			const TestData = require("../../datas/song.json");
			TestData.fullUrl = Utils.getLinks(
				Utils.decode_download_url(
					TestData.linkDownload[0],
					TestData.linkDownload[1],
					TestData.linkDownload[2]
				)
			);;
			this.setState({ status: "sucess", track: TestData ? TestData : null });
		} else {
			this.setState({ status: "loading" });
			Api.getSong(track.id, (data) => {
				if (data) {
					data.fullUrl = Utils.getLinks(
						Utils.decode_download_url(
							data.linkDownload[0],
							data.linkDownload[1],
							data.linkDownload[2]
						)
					);;
					this.setState({ status: "success", track: data });
				} else {
					this.setState({ status: "failed" });
				}
			});
		}
	}

	setDuration(data) {
		console.log('setDuration', data);
		this.setState({ totalLength: Math.floor(data.duration) });
	}

	setTime(data) {
		this.setState({ currentPosition: Math.floor(data.currentTime) });
	}

	seek(time) {
		time = Math.round(time);
		this.refs.audioElement && this.refs.audioElement.seek(time);
		this.setState({
			currentPosition: time,
			paused: false
		});
	}

	onBack() {
		if (this.state.currentPosition < 10 && this.state.selectedTrack > 0) {
			this.refs.audioElement && this.refs.audioElement.seek(0);
			this.setState({ isChanging: true });
			setTimeout( () => {
				this.setState({
					currentPosition: 0,
					paused: false,
					totalLength: 1,
					isChanging: false,
					selectedTrack: this.state.selectedTrack - 1
				});
				this.loadSong();
			}, 0);
		} else {
			this.refs.audioElement.seek(0);
			this.setState({
				currentPosition: 0
			});
		}
	}

	onForward() {
		if (this.state.selectedTrack < this.state.tracks.length - 1) {
			this.refs.audioElement && this.refs.audioElement.seek(0);
			this.setState({ isChanging: true });
			setTimeout(() => {
				this.setState({
					currentPosition: 0,
					totalLength: 1,
					paused: false,
					isChanging: false,
					selectedTrack: this.state.selectedTrack + 1,
				});
				this.loadSong();
			}, 0);
		}
	}

	onMorePress() {
		console.log(111);
	}

	onEnd() {
		
	}

	loadStart() {
	}

	render() {
		if (this.state.status == "") {
			return null;
		}
		if (this.state.status == "loading") {
			return (
				<View style={styles.container}>
					<Text>Đang tìm ...</Text>
				</View>
			);
		}
		if (this.state.status == "failed") {
			return (
				<View style={styles.container}>
					<Text>Không tìm thấy bài hát !</Text>
				</View>
			);
		}
		// console.log(this.state);
		console.log('xxx', this.refs);
		const track = this.state.isLocal ? Utils.covertSongInfo(this.state.track) : this.state.track;
		const video = this.state.isChanging ? null : (
			<Video
				source={{ uri: track.fullUrl[0].url }} // Can be a URL or a local file.
				ref="audioElement"
				playInBackground={true}
				playWhenInactive={true}
				paused={this.state.paused} // Pauses playback entirely.
				resizeMode="cover" // Fill the whole screen at aspect ratio.
				repeat={true} // Repeat forever.
				onLoadStart={this.loadStart} // Callback when video starts to load
				onLoad={this.setDuration.bind(this)} // Callback when video loads
				onProgress={this.setTime.bind(this)} // Callback every ~250ms with currentTime
				onEnd={this.onEnd} // Callback when playback finishes
				onError={this.videoError} // Callback when video cannot be loaded	
				style={styles.audioElement}
			/>
		);

		return (
			<View style={styles.container}>
				<StatusBar hidden={true} />
				<Header message="Playing From Charts" />
				<AlbumArt url={track.avatar} />
				<TrackDetails title={track.name} 
							artist={track.artist} 
							onMorePress={this.onMorePress.bind(this)} 
							urls={track.fullUrl} 
							isLocal={this.state.isLocal} />
				<SeekBar
					onSeek={this.seek.bind(this)}
					trackLength={this.state.totalLength}
					onSlidingStart={() => this.setState({ paused: true })}
					currentPosition={this.state.currentPosition}
				/>
				<Controls
					onPressRepeat={() =>
						this.setState({ repeatOn: !this.state.repeatOn })
					}
					repeatOn={this.state.repeatOn}
					shuffleOn={this.state.shuffleOn}
					forwardDisabled={
						this.state.selectedTrack === this.state.tracks.length - 1
					}
					onPressShuffle={() =>
						this.setState({ shuffleOn: !this.state.shuffleOn })
					}
					onPressPlay={() => this.setState({ paused: false })}
					onPressPause={() => this.setState({ paused: true })}
					onBack={this.onBack.bind(this)}
					onForward={this.onForward.bind(this)}
					paused={this.state.paused}
				/>
				{video}
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		backgroundColor: "rgb(4,4,4)"
	},
	audioElement: {
		height: 0,
		width: 0
	}
};
