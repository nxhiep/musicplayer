import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";

export default class TestView extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: navigation.getParam("item").name
		};
	};

	constructor(props) {
		super(props);
		console.log("props", props);
	}

	render() {
		return (
			<View style={styles.container}>
				<Text>Test View</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		justifyContent: "flex-start"
	}
});
