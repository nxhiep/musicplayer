import React, { Component } from "react";
import {
	Platform,
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableOpacity,
	FlatList,
	Image,
	StatusBar
} from "react-native";
import SongItem from "./SongItem";
import * as Api from "../../utils/Api";
import * as Utils from "../../utils/Utils";
import Icon from "react-native-vector-icons/dist/FontAwesome";

const containerHomeBorderTop = Platform.select({
	ios: { borderWith: 1 },
	android: { borderWith: 0 }
});

export default class SearchView extends Component {
	static navigationOptions = {
		header: null
	};

	constructor(props) {
		super(props);
		console.log('SearchView', props);
		
		this.state = {
			status: "",
			datas: null,
			name: Utils.isTest ? "tan cung cua noi nho" : "",
			currentIndex: -1
		};
		this.renderSongItem.bind(this);
	}

	componentDidMount() {
		if (Utils.isTest) {
			this.onSearch();
		}
	}

	onSearch() {
		if (Utils.isTest) {
			this.setState({ status: "loading" });
			const TestData = require("../../datas/data.json");
			this.setState({ status: "success", datas: TestData ? TestData : [] });
		} else {
			if (
				!this.state.name ||
				(this.state.name && this.state.name.length == 0)
			) {
				this.setState({ status: "name_empty" });
				return;
			}
			this.setState({ status: "loading" });
			Api.searchSongs(this.state.name, data => {
				if (!data) {
					this.setState({ status: "failed" });
				} else if (data.length == 0) {
					this.setState({ status: "empty" });
				} else {
					console.log(data);
					this.setState({ status: "success", datas: data });
				}
			});
		}
	}

	renderSongItem(item) {
		return (
			<SongItem
				item={item}
				tracks={this.state.datas}
				navigation={this.props.navigation}
			/>
		);
	}

	onEndReached() {
		console.log('onEndReached');
	}

	onRefresh() {
		this.onSearch()
	}

	renderResult(datas, status) {
		if (status == "") {
			return null;
		}
		if (status == "name_empty") {
			return (
				<View style={styles.statusPanelStyle}>
					<Text style={styles.textColor}>Chưa nhập tên bài hát !</Text>
				</View>
			);
		}
		if (status == "loading") {
			return (
				<View style={styles.statusPanelStyle}>
					<Text style={styles.textColor}>Đang tìm ...</Text>
				</View>
			);
		}
		if (status == "failed" || status == "empty") {
			return (
				<View style={styles.statusPanelStyle}>
					<Text style={styles.textColor}>Không tìm thấy bài hát !</Text>
				</View>
			);
		}
		return (
			<FlatList
				style={{ padding: 5, height: "100%" }}
				keyExtractor={item => item.id}
				data={datas}
				renderItem={({ item }) => this.renderSongItem(item)}
				onEndReached={this.onEndReached.bind(this)}
				onEndReachedThreshold={0.5}
				onRefresh={this.onRefresh.bind(this)}
				refreshing={false}
			/>
		);
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<View style={styles.container}>
					<Image style={{ flex: 1 }} source={require("../../images/bg-1.jpg")} />
				</View>
				<StatusBar hidden={true} />
				<View style={styles.content}>
					<View style={[styles.containerHome, containerHomeBorderTop]}>
						<View style={styles.searchContainer}>
							<TextInput
								placeholder="Nhập tên bài hát"
								placeholderTextColor="white"
								style={styles.searchBox}
								onChangeText={text => this.setState({ name: text })}
								onSubmitEditing={this.onSearch.bind(this)}
								value={this.state.name}
							/>
							<TouchableOpacity
								onPress={this.onSearch.bind(this)}
								style={styles.btnSearch}
							>
								<Icon name="search" size={18} color={"white"} />
							</TouchableOpacity>
						</View>
						{this.renderResult(this.state.datas, this.state.status)}
						{/* <TouchableHighlight>
              <Text>load more</Text>
            </TouchableHighlight> */}
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		justifyContent: "center",
		alignContent: "center"
	},
	content: {
		flex: 1,
		backgroundColor: "rgba(0, 0, 0, 0.5)"
	},
	containerHome: {
		flex: 1,
		// borderTopWidth: 1,
		borderColor: "white",
		flexDirection: "column",
		flexWrap: "wrap"
	},
	searchContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignContent: "center",
		padding: 5,
		// borderBottomWidth: 1,
		borderColor: "white",
		flexWrap: "wrap"
	},
	searchBox: {
		flex: 1,
		height: 40,
		borderColor: "white",
		borderWidth: 1,
		paddingLeft: 10,
		color: "white"
	},
	textColor: {
		color: "white"
	},
	statusPanelStyle: {
		padding: 10,
		textAlign: "center"
	},
	btnSearch: {
		backgroundColor: "transparent",
		alignItems: "center",
		padding: 10,
		borderWidth: 1,
		borderColor: "white",
		marginLeft: 5
	}
});
