import React, { Component } from "react";
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Linking,
	Image
} from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";

export default class SongItem extends Component {
	constructor(props) {
		super(props);
	}

	onOpenLink(url) {
		Linking.canOpenURL(url).then(supported => {
			if (supported) {
				Linking.openURL(url);
			} else {
				console.log("Don't know how to open URI: " + item.url);
			}
		});
	}

	render() {
		let { name, singer, link } = this.props.item;
		let selectedTrack = this.props.tracks.indexOf(this.props.item);
		return (
			<View style={styles.songItem}>
				<TouchableOpacity
					style={styles.songInfo}
					onPress={() => {
						this.props.navigation.navigate("Player", {
							...this.props,
							isLocal: false,
							selectedTrack: selectedTrack,
							play: false
						})
					}}
				>
					<View>
						<Text style={styles.songItemText}>{name}</Text>
						<Text style={[styles.songItemText, { fontStyle: "italic" }]}>
							{singer}
						</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.playBtn}
					onPress={() => {
						this.props.navigation.navigate("Player", {
							...this.props,
							isLocal: false,
							selectedTrack: selectedTrack,
							play: true
						})
					}}
				>
					<Text>
						<Icon name="play" style={{ fontSize: 20, color: "white" }} />
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					style={styles.playBtn}
					onPress={() => this.onOpenLink(link)}
				>
					<Text>
						<Icon name="download" style={{ fontSize: 20, color: "white" }} />
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	songItem: {
		padding: 5,
		borderWidth: 1,
		borderColor: "white",
		marginBottom: 5,
		alignItems: "center",
		flexDirection: "row",
		justifyContent: "space-between",
		flexWrap: "wrap"
	},
	songInfo: {
		flex: 1,
		alignItems: "flex-start",
		flexDirection: "column"
	},
	songItemText: {
		color: "white"
	},
	playBtn: {
		width: 40,
		alignItems: "center"
	}
});
