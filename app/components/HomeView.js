import React, { Component } from "react";
import { Text, View, Platform, FlatList, StyleSheet, Image, TouchableOpacity } from "react-native";
import Permissions from "react-native-permissions";
import MusicFiles from "react-native-get-music-files";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import * as Api from '../../utils/Api';

const isAndroid = Platform.OS == "android";

export default class HomeView extends Component {
	constructor(props) {
		super(props);
		console.log("HomeView", props);
		this.state = {
			songPermission: null,
			tracks: null, 
			status: 'request-permissions'
		};
	}

	componentDidMount() {
		this.requestPermission();
	}

	requestPermission(){
		if(isAndroid){
			Permissions.request("storage", {type: "always"})
			.then(response => {
				this.setState({ songPermission: response, status: 'loading' });
				this.loadMusicFiles();
			});
		} else {
			Permissions.request("mediaLibrary", {type: "always"})
			.then((response) => {
				this.setState({ songPermission: response, status: 'loading' });
				this.loadMusicFiles();
			});
		}
	}
	
	
	loadMusicFiles(){
		let key = 'tracks';
		Api.getStorageItem(key).then((data) => {
			if(!data || (typeof data) !== 'object'){
				MusicFiles.getAll({
					id: true,
					blured: true, // works only when 'cover' is set to true
					artist: true,
					duration: true, //default : true
					cover: false, //default : true,
					genre: true,
					title: true,
					cover: true,
					minimumSongDuration: 10000, // get songs bigger than 10000 miliseconds duration,
					fields: [
						"title",
						"albumTitle",
						"genre",
						"lyrics",
						"artwork",
						"duration"
					] // for iOs Version
				})
				.then(tracks => {
					console.log("tracks", tracks);
					if(tracks && ((typeof tracks) === 'object' || (typeof tracks) === 'array')){
						this.setState({ tracks: tracks, status: 'success' });
						Api.setStorageItem(key, JSON.stringify(tracks));
					} else {
						this.setState({ tracks: [], status: 'success' });
					}
				})
				.catch(error => {
					console.log("error", error);
				});
			} else {
				this.setState({ tracks: JSON.parse(data), status: 'success' });
			}
		});
	}

	renderSongItem(item) {
		let selectedTrack = this.state.tracks.indexOf(item);
		return (
			<View style={styles.itemContainer}>
				<TouchableOpacity 
					style={styles.songInfo}
					onPress={() => {
						console.log('view selected ', item);
						this.props.navigation.navigate("Player", {
							...this.props,
							tracks: this.state.tracks,
							item: item,
							isLocal: true,
							selectedTrack: selectedTrack,
							play: false
						})
					}}
				>
					<View>
						<Text style={styles.textColor}>{ item.title ? item.title : item.fileName }</Text>
						<Text style={styles.textColor}>{item.author}</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity style={styles.playBtn}
					onPress={() => {
						console.log('selected ', item)
						this.props.navigation.navigate("Player", {
							...this.props,
							tracks: this.state.tracks,
							item: item,
							isLocal: true,
							selectedTrack: selectedTrack,
							play: true
						})
					}}
				>
					<Icon name="play" style={{ fontSize: 20, color: "white" }} />
				</TouchableOpacity>
			</View>
		);
	}

	render() {
		if(this.state.status == 'request-permissions'){
			return (
				<View style={{ flex: 1 }}>
					<Text>Don't not permissions</Text>
					<TouchableOpacity onPress={() => {
						this.requestPermission.bind(this)
					}}>
						<Text>Request again</Text>
					</TouchableOpacity>
				</View>
			);
		}
		if(this.state.status == 'loading'){
			return (
				<View style={{ flex: 1 }}>
					<Text>Loading...</Text>
				</View>
			);
		}
		if(this.state.status == 'success' && this.state.tracks){
			return (
				<View style={{ flex: 1 }}>
					<View style={styles.container}>
						<Image style={{ flex: 1 }} source={require("../../images/bg-1.jpg")} />
					</View>
					<View style={styles.content}>
						<View style={styles.containerHome}>
							<FlatList
								style={{ padding: 5, height: "100%" }}
								keyExtractor={item => item.id}
								data={this.state.tracks}
								renderItem={({ item }) => this.renderSongItem(item)}
							/>
						</View>
					</View>
				</View>
			);
		}
		return null;
	}
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		justifyContent: "center",
		alignContent: "center"
	},
	content: {
		flex: 1,
		backgroundColor: "rgba(0, 0, 0, 0.5)"
	},
	containerHome: {
		flex: 1,
		borderColor: "white",
		flexDirection: "column",
		flexWrap: "wrap"
	},
	textColor: {
		color: 'white'
	},
	itemContainer: { 
		padding: 10, 
		borderWidth: 1, 
		borderColor: 'white', 
		alignItems: "center", 
		flexDirection: "row", 
		justifyContent: "space-between",
		marginBottom: 10
	},
	playBtn: {
		width: 40,
		alignItems: "center"
	},
	songInfo: {
		flex: 1
	}
});
