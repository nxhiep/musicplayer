import { AsyncStorage } from "react-native";

export function getSong(key, callback) {
	fetch('http://apichiasenhac.appspot.com/api/music?url=' + key + '.html')
	.then((response) => response.json())
	.then((data) => {
		callback(data ? data : null);
	})
	.catch((error) => {
		console.error(error);
		callback(null)
	});
}

export function searchSongs(name, callback) {
	fetch('http://apichiasenhac.appspot.com/api/search?name=' + name)
	.then((response) => response.json())
	.then((data) => {
		callback(data ? data : []);
	})
	.catch((error) => {
		console.error(error);
		callback(null);
	});
}

let PLAYER = null;
export function setPlayer(player) {
	PLAYER = player;
}

export function getPlayer() {
	return PLAYER;
}

export async function setStorageItem(key, data){
	try {
		await AsyncStorage.setItem(key, data);
		// console.log('setItem', value);
	} catch(error){
		console.log(error);
	}
}

export async function getStorageItem(key) {
	try {
		const value = await AsyncStorage.getItem(key);
		// console.log('getItem', value);
		return value;
	} catch (error) {
		// Handle errors here
		console.log(error);
	}
}